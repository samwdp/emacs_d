;; -*- lexical-binding: t; -*-
;; The default is 800 kilobytes.  Measured in bytes.

(setq gc-cons-threshold (* 50 1000 1000))

;; Prrofile emacs startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded %d modules in %s seconds with %d garbage collections."
                     (length load-path)
                     (emacs-init-time "%.2f")
                     gcs-done)))

;; Silence compiler warnings as they can be pretty disruptive
(setq native-comp-async-report-warnings-errors nil)

;; Set the right directory to store the native comp cache
(add-to-list 'native-comp-eln-load-path (expand-file-name "eln-cache/" user-emacs-directory))

(setq inhibit-startup-message t)
(add-to-list 'default-frame-alist '(fullscreen . maximized))
(scroll-bar-mode -1)        ; Disable visible scrollbar
(tool-bar-mode -1)          ; Disable the toolbar
(tooltip-mode -1)           ; Disable tooltips
(set-fringe-mode 10)        ; Give some breathing room
(setq make-backup-files nil)
(setq create-lockfiles nil)
(menu-bar-mode -1)            ; Disable the menu bar
(setq visible-bell t)
(prefer-coding-system       'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)

(setq-default tab-width 2)
(setq-default evil-shift-width tab-width)
(setq-default indent-tabs-mode nil)

(defvar font-size 16)

(defun sp/set-font-faces ()
  (set-face-attribute 'default nil :font (font-spec :family "NotoSansMono Nerd Font" :size font-size))
  (set-face-attribute 'fixed-pitch nil :font (font-spec :family "NotoSansMono Nerd Font" :size font-size))
  (set-face-attribute 'fixed-pitch-serif nil :font (font-spec :family "NotoSansMono Nerd Font" :size font-size))
  (set-face-attribute 'variable-pitch nil :font (font-spec :family "NotoSans Nerd Font" :size font-size))
  (message "setup fonts"))

(defun sp/setup-unicode (&optional frame)
  (when (and frame (display-graphic-p frame))
    (add-to-list 'load-path "~/.emacs.d/straight/build/list-utils/")
    (add-to-list 'load-path "~/.emacs.d/straight/build/ucs-utils/")
    (add-to-list 'load-path "~/.emacs.d/straight/build/font-utils/")
    (add-to-list 'load-path "~/.emacs.d/straight/build/unicode-fonts/")
    (require 'unicode-fonts)
    (unicode-fonts-setup))
  (message "setup unicode"))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (setq doom-modeline-icon t)
                (with-selected-frame frame
                  (progn (sp/set-font-faces)
                         (sp/setup-unicode (selected-frame))
                         (display-battery-mode 1)))))
  (progn
    (sp/set-font-faces)
    (sp/setup-unicode (selected-frame))
    (display-battery-mode 1)))
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

(setq-default prettify-symbols-alist
              '(("#+BEGIN_SRC" . "»")
                ("#+END_SRC" . "«")
                ("#+begin_src" . "»")
                ("#+end_src" . "«")))

(setq prettify-symbols-unprettify-at-point 'right-edge)

(use-package unicode-fonts)

(defvar localleader "SPC m")

(defun sp/localleader (extension)
  (concat localleader extension))

(use-package general
  :after evil
  :config
  (general-create-definer sp/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC")
  (sp/leader-keys
    "SPC" '(counsel-M-x :which-key "files")
    ";" '(eval-expression :whick-key "eval epression")
    "m" '(:ignore t :which-key "localleader")
    ;; workspaces
    "<tab>" '(:ignore t :which-key "workspaces")
    "<tab><tab>" '(persp-switch :which-key "list")
    "<tab>n" '(persp-next :which-key "next")
    "<tab>p" '(persp-prev :which-key "previous")
    "<tab>d" '(persp-kill :which-key "delete")
    ;; buffers
    "b" '(:ignore t :which-key "buffers")
    "bb" '(persp-ivy-switch-buffer :which-key "buffer list")
    "bd" '(evil-delete-buffer :which-key "kill")
    "c" '(:ignore t :which-key "code")
    "i" '(:ignore t :which-key "insert")
    ;; version control
    "g" '(:ignore t :which-key "git")
    "gg" '(magit-status :which-key "status")
    ;; projects
    "p" '(:ignore t :which-key "projects")
    "pp" '(projectile-switch-project :which-key "list projects")
    "pi" '(projectile-invalidate-cache :which-key "invalidate cache")
    ;; open applications
    "o" '(:ignore t :which-key "open")
    "op" '(treemacs :which-key "side draw")
    ;; files
    "f" '(:ignore t :which-key "files")
    "fd" '(delete-file :which-key "delete")
    "fn" '(counsel-find-file :which-key "new")
    "fo" '(sp/format :which-key "format")
    "fs" '(save-buffer :which-key "save")
    "ff" '(counsel-projectile-find-file :which-key "find in proj")
    "fF" '(counsel-find-file :which-key "find")
    ;; window
    "w" '(:ignore t :which-key "window")
    "wh" '(evil-window-left :which-key "left")
    "wj" '(evil-window-down :which-key "down")
    "wk" '(evil-window-up :which-key "up")
    "wl" '(evil-window-right :which-key "right")
    "wd" '(evil-window-delete :which-key "delete")))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)
  (define-key evil-normal-state-map (kbd "C-h") 'evil-window-left)
  (define-key evil-normal-state-map (kbd "C-j") 'evil-window-down)
  (define-key evil-normal-state-map (kbd "C-k") 'evil-window-up)
  (define-key evil-normal-state-map (kbd "C-l") 'evil-window-right)
  (define-key evil-insert-state-map (kbd "C-.") 'company-complete)
  (define-key evil-normal-state-map (kbd "C-=") 'text-scale-increase)
  (define-key evil-normal-state-map (kbd "C--") 'text-scale-decrease)

  ;; Use visual line motions even outside of visual-line-mode buffers
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package doom-themes
  :init (load-theme 'gruvbox t))

(use-package hide-mode-line)

(use-package doom-modeline
  :hook (after-init . doom-modeline-mode)
  :hook (doom-modeline-mode . column-number-mode)
  :init
  (unless after-init-time
    ;; prevent flash of unstyled modeline at startup
    (setq-default mode-line-format nil))
  ;; We display project info in the modeline ourselves
  (setq projectile-dynamic-mode-line nil)
  ;; Set these early so they don't trigger variable watchers
  (setq doom-modeline-bar-width 3
        doom-modeline-height 15
        doom-modeline-icon t
        doom-modeline-github nil
        doom-modeline-mu4e nil
        doom-modeline-persp-name nil
        doom-modeline-minor-modes nil
        doom-modeline-major-mode-icon nil
        doom-modeline-buffer-file-name-style 'relative-from-project
        ;; Only show file encoding if it's non-UTF-8 and different line endings
        ;; than the current OSes preference
        doom-modeline-buffer-encoding 'nondefault
        doom-modeline-default-eol-type
        (cond ((eq system-type 'darwin) 2)
              ((eq system-type 'windows-nt) 1)
              (0)))
  (when (daemonp)
    (setq doom-modeline-icon t))
  :config
  (add-hook 'magit-mode-hook
             (defun +modeline-hide-in-non-status-buffer-h ()
               "Show minimal modeline in magit-status buffer, no modeline elsewhere."
               (if (eq major-mode 'magit-status-mode)
                   (doom-modeline-set-vcs-modeline)
                 (hide-mode-line-mode)))))

(use-package which-key
  :straight t
  :custom
  (which-key-show-transient-maps t)
  :config
  (which-key-mode))

(use-package popwin
  :init (popwin-mode 1))

(use-package prescient)
(use-package ivy-prescient
  :hook (ivy-mode . ivy-prescient-mode)
  :hook (ivy-prescient-mode . prescient-persist-mode)
  :init
  (setq prescient-filter-method '(literal regexp initialism fuzzy)))

(use-package company-prescient)

(use-package counsel
  :bind (("C-M-j" . 'counsel-switch-buffer)
	 :map minibuffer-local-map
	 ("C-r" . 'counsel-minibuffer-history))
  :config
  (counsel-mode 1))

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (setq ivy--regex-function 'ivy--regex-fuzzy)
  (require 'counsel nil t)
  (setq 
        ivy-wrap t
        ivy-fixed-height-minibuffer t
        ivy-read-action-function #'ivy-hydra-read-action
        ivy-read-action-format-function #'ivy-read-action-format-columns
        ;; don't show recent files in switch-buffer
        ivy-use-virtual-buffers nil
        ;; ...but if that ever changes, show their full path
        ivy-virtual-abbreviate 'full
        ;; don't quit minibuffer on delete-error
        ivy-on-del-error-function #'ignore
        ;; enable ability to select prompt (alternative to `ivy-immediate-done')
        ivy-use-selectable-prompt t)

  (ivy-mode 1))

(use-package all-the-icons-ivy
  :ensure t
  :init
  (setq all-the-icons-spacer " ")
  (add-hook 'after-init-hook 'all-the-icons-ivy-setup))

(use-package ivy-rich
  :after ivy
  :init
  (ivy-rich-mode 1))

(use-package ivy-posframe
  :after ivy
  :config
  (setq ivy-posframe-display-functions-alist '((t . ivy-posframe-display)))
  (setq ivy-posframe-border-width 3)
  (setq ivy-posframe-parameters
        '((left-fringe . 8)
          (top-fringe . 8)
          (bottom-fringe . 8)
          (right-fringe . 8)))
  :init
  (ivy-posframe-mode 1))

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(use-package mmm-mode
  :config
  (mmm-mode 1))

(use-package dired
  :straight nil
  :commands (dired dired-jump)
  :general
  (:states 'normal :keymaps 'dired-mode-map
           "h" 'dired-single-up-directory
           "l" 'dired-single-buffer
           "SPC" nil)
  :bind (("C-x C-j" . dired-jump))
  :custom ((dired-listing-switches "-agho --group-directories-first")))

(use-package dired-single
  :commands (dired dired-jump))

(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package dired-rsync
  :general
  (dired-mode-map "C-c C-r" #'dired-rsync))

(use-package diff-hl
  :hook (dired-mode . diff-hl-dired-mode-unless-remote)
  :hook (magit-post-refresh . diff-hl-magit-post-refresh)
  :config
  ;; use margin instead of fringe
  (diff-hl-margin-mode))

(use-package fd-dired
  :defer t
  :init
  (global-set-key [remap find-dired] #'fd-dired))

(use-package  all-the-icons
  :commands (all-the-icons-octicon
             all-the-icons-faicon
             all-the-icons-fileicon
             all-the-icons-wicon
             all-the-icons-material
             all-the-icons-alltheicon))

(use-package company
  :hook (after-init . global-company-mode)
  :bind
  (:map company-active-map
        ("<tab>" . company-complete-selection))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package projectile
  :diminish projectile-mode
  :config
  (projectile-mode)
  (setq projectile-project-root-files-bottom-up
        (append '(".projectile"  ; projectile's root marker
                  ".project"     ; doom project marker
                  ".git")        ; Git VCS root dir
                (when (executable-find "hg")
                  '(".hg"))      ; Mercurial VCS root dir
                (when (executable-find "bzr")
                  '(".bzr")))    ; Bazaar VCS root dir
        ;; This will be filled by other modules. We build this list manually so
        ;; projectile doesn't perform so many file checks every time it resolves
        ;; a project's root -- particularly when a file has no project.
        projectile-project-root-files '()
        projectile-project-root-files-top-down-recurring '("Makefile"))

  (setq projectile-git-submodule-command nil
        projectile-indexing-method 'hybrid)


  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  ;; NOTE: Set this to the folder where you keep your Git repos!
  (when (file-directory-p "w:/")
    (setq projectile-project-search-path '("w:/foresolutions"
                                           "w:/personal"
                                           "w:/personal/c"
                                           "w:/personal/rust"
                                           "w:/personal/cpp")))
  (setq projectile-switch-project-action #'projectile-dired))

(use-package counsel-projectile
  :after projectile
  :config (counsel-projectile-mode))

(use-package magit
  :commands magit-status
  :init
  (setq magit-auto-revert-mode nil)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package evil-magit
  :after '(magit evil))

;; NOTE: Make sure to configure a GitHub token before using this package!
;; - https://magit.vc/manual/forge/Token-Creation.html#Token-Creation
;; - https://magit.vc/manual/ghub/Getting-Started.html#Getting-Started

(use-package forge
  :after magit)

(use-package magit-gitflow
  :after (magit)
  :hook (magit-mode . turn-on-magit-gitflow))

(use-package magit-todos
  :after magit)

(use-package github-review
  :after magit)

(use-package perspective
  :config
  (persp-mode))

(use-package persp-projectile)

(use-package git-gutter
  :init (global-git-gutter-mode 1))

(use-package treemacs
  :general
  (:keymaps 'treemacs-mode-map
            "SPC" nil
            "C-h" #'evil-window-left
            "C-j" #'evil-window-down
            "C-k" #'evil-window-up
            "C-l" #'evil-window-right)
  :config
  (setq treemacs-position 'right
        treemacs-width 30))

(use-package treemacs-evil
  :after (treemacs evil)
  :straight t)

(use-package treemacs-projectile
  :after (treemacs projectile)
  :straight t)

(use-package treemacs-magit
  :after (treemacs magit)
  :straight t)

(use-package treemacs-perspective ;;treemacs-perspective if you use perspective.el vs. persp-mode
  :after (treemacs perspective) ;;or perspective vs. persp-mode
  :straight t
  :config (treemacs-set-scope-type 'Perspectives))

(use-package lsp-treemacs
  :after (treemacs lsp))

(use-package tree-sitter
  :straight t)

(use-package tree-sitter-langs
  :straight t)

(use-package tree-sitter-indent
  :straight t)

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :hook (prog-mode . rainbow-mode)
  :init (rainbow-mode 1))

(use-package smartparens
  :hook (smartparens-mode . rainbow-mode)
  :init (smartparens-global-mode 1)
  :config
  (add-hook 'eval-expression-minibuffer-setup-hook
            (defun init-smartparens ()
              (when smartparens-global-mode
                (progn (smartparens-mode +1)
                       (rainbow-delimiters-mode +1)))))
  (add-hook 'minibuffer-setup-hook
            (defun smartparens-minibuffer-hook ()
              (smartparens-mode +1)
              (rainbow-delimiters-mode +1))))

(use-package language-id
  :config
  (defconst language-id--definitions
    '(

    ;;; Definitions that need special attention to precedence order.

      ;; It is not uncommon for C++ mode to be used when writing Cuda.
      ;; In this case, the only way to correctly identify Cuda is by
      ;; looking at the extension.
      ("Cuda"
       (c++-mode (language-id--file-name-extension ".cu"))
       (c++-mode (language-id--file-name-extension ".cuh")))

      ;; json-mode is derived from javascript-mode.
      ("JSON"
       json-mode
       (web-mode (web-mode-content-type "json") (web-mode-engine "none")))

      ;; php-mode is derived from c-mode.
      ("PHP" php-mode)

      ;; scss-mode is derived from css-mode.
      ("SCSS" scss-mode)

      ;; svelte-mode is derived from html-mode.
      ("Svelte"
       svelte-mode
       (web-mode (web-mode-content-type "html") (web-mode-engine "svelte")))

      ;; terraform-mode is derived from hcl-mode.
      ("Terraform" terraform-mode)

      ;; TypeScript/TSX need to come before JavaScript/JSX because in
      ;; web-mode we can tell them apart by file name extension only.
      ;;
      ;; This implies that we inconsistently classify unsaved temp
      ;; buffers using TypeScript/TSX as JavaScript/JSX.
      ("TSX"
       typescript-tsx-mode
       (web-mode
        (web-mode-content-type "jsx")
        (web-mode-engine "none")
        (language-id--file-name-extension ".tsx")))
      ("TypeScript"
       typescript-mode
       (web-mode
        (web-mode-content-type "javascript")
        (web-mode-engine "none")
        (language-id--file-name-extension ".ts")))

      ;; ReScript needs to come before Reason because in reason-mode
      ;; we can tell them apart by file name extension only.
      ("ReScript"
       (reason-mode
        (language-id--file-name-extension ".res")))
      ("ReScript"
       (reason-mode
        (language-id--file-name-extension ".resi")))
      ("Reason" reason-mode)

      ;; vue-html-mode is derived from html-mode.
      ("Vue"
       vue-mode
       vue-html-mode
       (web-mode (web-mode-content-type "html") (web-mode-engine "vue")))

    ;;; The rest of the definitions are in alphabetical order.

      ("Assembly" asm-mode nasm-mode)
      ("ATS" ats-mode)
      ("Awk" awk-mode)
      ("Bazel" bazel-mode)
      ("BibTeX" bibtex-mode)
      ("C" c-mode)
      ("C#" csharp-mode csharp-tree-sitter-mode)
      ("C++" c++-mode)
      ("Cabal Config" haskell-cabal-mode)
      ("Clojure" clojurescript-mode clojurec-mode clojure-mode)
      ("CMake" cmake-mode)
      ("Common Lisp" lisp-mode)
      ("Crystal" crystal-mode)
      ("CSS"
       css-mode
       (web-mode (web-mode-content-type "css") (web-mode-engine "none")))
      ("Cuda" cuda-mode)
      ("D" d-mode)
      ("Dart" dart-mode)
      ("Dhall" dhall-mode)
      ("Dockerfile" dockerfile-mode)
      ("Elixir" elixir-mode)
      ("Elm" elm-mode)
      ("Emacs Lisp" emacs-lisp-mode)
      ("F#" fsharp-mode)
      ("Fish" fish-mode)
      ("Fortran" fortran-mode)
      ("Fortran Free Form" f90-mode)
      ("GLSL" glsl-mode)
      ("Go" go-mode)
      ("GraphQL" graphql-mode)
      ("Haskell" haskell-mode)
      ("HCL" hcl-mode)
      ("HTML"
       html-helper-mode mhtml-mode html-mode nxhtml-mode
       (web-mode (web-mode-content-type "html") (web-mode-engine "none")))
      ("Java" java-mode)
      ("JavaScript"
       (js-mode (flow-minor-mode nil))
       (js2-mode (flow-minor-mode nil))
       (js3-mode (flow-minor-mode nil))
       (web-mode (web-mode-content-type "javascript") (web-mode-engine "none")))
      ("JSON"
       json-mode
       (web-mode (web-mode-content-type "json") (web-mode-engine "none")))
      ("Jsonnet" jsonnet-mode)
      ("JSX"
       js2-jsx-mode jsx-mode rjsx-mode react-mode
       (web-mode (web-mode-content-type "jsx") (web-mode-engine "none")))
      ("Kotlin" kotlin-mode)
      ("LaTeX" latex-mode)
      ("Less" less-css-mode)
      ("Literate Haskell" literate-haskell-mode)
      ("Lua" lua-mode)
      ("Markdown" gfm-mode markdown-mode)
      ("Nix" nix-mode)
      ("Objective-C" objc-mode)
      ("OCaml" caml-mode tuareg-mode)
      ("Perl" cperl-mode perl-mode)
      ("Protocol Buffer" protobuf-mode)
      ("PureScript" purescript-mode)
      ("Python" python-mode)
      ("R" ess-r-mode (ess-mode (ess-dialect "R")))
      ("Racket" racket-mode)
      ("Ruby" enh-ruby-mode ruby-mode)
      ("Rust" rust-mode rustic-mode)
      ("Scala" scala-mode)
      ("Scheme" scheme-mode)
      ("Shell" sh-mode)
      ("Solidity" solidity-mode)
      ("SQL" sql-mode)
      ("Swift" swift-mode swift3-mode)
      ("TOML" toml-mode conf-toml-mode)
      ("V" v-mode)
      ("Verilog" verilog-mode)
      ("XML"
       nxml-mode xml-mode
       (web-mode (web-mode-content-type "xml") (web-mode-engine "none")))
      ("YAML" yaml-mode))
    "Internal table of programming language definitions."))

(defun sp/format ()
  "Uses lsp-format-buffer if LSP is active or uses format-all-buffer if there is no active LSP"
  (interactive)
  (let ((active-modes))
    (mapc (lambda (mode) (condition-case nil
                             (if (and (symbolp mode) (symbol-value mode))
                                 (add-to-list 'active-modes mode))
                           (error nil)))
          minor-mode-list)
    (if (memq 'lsp-mode active-modes)
        (lsp-format-buffer)
      (format-all-buffer (current-buffer)))))


(use-package format-all
  :straight t)

(defvar csharp-format--binary "dotnet-format")
(defun csharp-format--install ()
  (message "install"))
(defun csharp-format--run ()
  (message "run"))
(defun csharp-format (project)
  (if (executable-find csharp-format--binary)
      (csharp-format--run)
    (csharp-format--install)
    (csharp-format--run)))

(use-package clang-format)

(use-package evil-nerd-commenter
  :bind ("C-/" . evilnc-comment-or-uncomment-lines))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :general
  (:keymaps 'lsp-mode-map :states 'normal
            "C-<return>" #'(lsp-ui-sideline-apply-code-actions :which-key "actions")
            "SPC cd" #'(lsp-find-definition :which-key "definition")
            "SPC cl" #'(:ignore t :which-key "lsp")
            "SPC clw" #'(:ignore t :which-key "window")
            "SPC clwr" #'(lsp-workspace-restart :which-key "restart")
            "SPC ci" #'(lsp-find-implementation :which-key "implementation"))
  :init
  (setq lsp-keymap-prefix "C-c l")  ;; Or 'C-l', 's-l'
  :config
  (lsp-enable-which-key-integration t))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :general
  (:states 'normal :keymaps 'lsp-ui-peek-mode-map
           "j" #'(lsp-ui-peek--select-next)
           "k" #'(lsp-ui-peek--select-prev)
           "C-j" #'(lsp-ui-peek--select-next-file)
           "C-k" #'(lsp-ui-peek--select-prev-file))
  :custom
  (setq lsp-ui-peek-enable t
        lsp-ui-doc-max-height 8
        lsp-ui-doc-max-width 72         ; 150 (default) is too wide
        lsp-ui-doc-delay 0.75           ; 0.2 (default) is too naggy
        lsp-ui-doc-show-with-mouse nil  ; don't disappear on mouseover
        lsp-ui-doc-position 'top-right-corner
        lsp-ui-sideline-ignore-duplicate t
        ;; Don't show symbol definitions in the sideline. They are pretty noisy,
        ;; and there is a bug preventing Flycheck errors from being shown (the
        ;; errors flash briefly and then disappear).
        lsp-ui-sideline-show-hover nil
        ;; Re-enable icon scaling (it's disabled by default upstream for Emacs
        ;; 26.x compatibility; see emacs-lsp/lsp-ui#573)
        lsp-ui-sideline-actions-icon lsp-ui-sideline-actions-icon-default))

(use-package lsp-ivy
  :after (ivy lsp))

(use-package dap-mode
  :commands dap-debug
  :hook (dap-mode . dap-tooltip-mode)
  :config
  (require 'dap-node)
  (require 'dap-netcore)
  (require 'dap-lldb)
  (require 'dap-cpptools)
  (setq dap-netcore-install-dir "c:/tools/"))

(use-package dap-ui
  :straight nil
  :after dap-mode
  :hook (dap-mode . dap-ui-mode)
  :hook (dap-ui-mode . dap-ui-controls-mode))

(use-package yasnippet
  :straight t
  :defer 0.1
  :custom
  (yas-prompt-functions '(yas-completing-prompt))
  :config
  (sp/leader-keys
    "is" '(yas-insert-snippet :which-key "snippet"))
  (yas-reload-all)
  (yas-global-mode 1))

(use-package doom-snippets
  :after yasnippet
  :straight (doom-snippets :type git :host github :repo "hlissner/doom-snippets" :files ("*.el" "*")))

(defun efs/configure-eshell ()
  ;; Save command history when commands are entered
  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)

  ;; Truncate buffer for performance
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)

  ;; Bind some useful keys for evil-mode
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "C-r") 'counsel-esh-history)
  (evil-define-key '(normal insert visual) eshell-mode-map (kbd "<home>") 'eshell-bol)
  (evil-normalize-keymaps)

  (setq eshell-history-size         10000
        eshell-buffer-maximum-lines 10000
        eshell-hist-ignoredups t
        eshell-scroll-to-bottom-on-input t))

(use-package eshell-git-prompt
  :after eshell)

(use-package eshell
  :hook (eshell-first-time-mode . efs/configure-eshell)
  :hook (eshell-mode . smartparens-mode)
  :general
  (:states '(normal insert) :keymaps 'eshell-mode-map
           "C-h" #'evil-window-left
           "C-j" #'evil-window-down
           "C-k" #'evil-window-up
           "C-l" #'evil-window-right)
  :config
  (eshell-git-prompt-use-theme 'multiline))

(use-package eshell-toggle
  :general
  (:states 'normal
           "SPC oe" #'(eshell-toggle :which-key "eshell"))
  :custom
  (eshell-toggle-size-fraction 3)
  (eshell-toggle-use-projectile-root t)
  (eshell-toggle-run-command nil))

(defun run-powershell ()
  "Run powershell"
  (interactive)
  (async-shell-command "c:\\Program Files\\PowerShell\\7\\pwsh.exe -Command -"
               nil
               nil))
  (when (eq system-type 'windows-nt)
    (setq explicit-shell-file-name "c:/Program Files/PowerShell/7/pwsh.exe"))

(use-package elisp-mode
  :straight nil
  :general
  (:states 'normal
           :keymaps 'emacs-lisp-mode-map
           (sp/localleader "e") '(:ignore t :which-key "eval")
           (sp/localleader "eb") '(eval-buffer :which-key "buffer"))
  :hook (emacs-lisp-mode . company-mode)
  :hook (emacs-lisp-mode . smartparens-mode))

(use-package csharp-mode
  :mode (("\\.cs\\'" . csharp-tree-sitter-mode)
         ("\\.csx\\'" . csharp-tree-sitter-mode))
  :hook (csharp-tree-sitter-mode . smartparens-mode)
  :hook (csharp-tree-sitter-mode . lsp-deferred))

(use-package sharper
  :general
  (:states 'normal
	 :keymaps 'csharp-tree-sitter-mode-map
           (sp/localleader "s") '(sharper-main-transient :which-key "sharper")))

(use-package powershell
  :hook (powershell-mode . smartparens-mode)
  :hook (powershell-mode . lsp-deferred))

(use-package typescript-mode
  :hook (typescript-mode . smartparens-mode)
  :hook (typescript-mode . lsp-deferred))

(use-package js-mode
  :straight nil
  :hook (js-mode . smartparens-mode)
  :hook (js-mode . lsp-deferred))

(use-package css-mode
  :straight nil
  :hook (css-mode . smartparens-mode)
  :hook (css-mode . lsp-deferred))

(use-package sass-mode
  :hook (sass-mode . smartparens-mode)
  :hook (sass-mode . lsp-deferred))

(use-package less-mode
  :straight nil
  :hook (less-mode . smartparens-mode)
  :hook (less-mode . lsp-deferred))

(use-package web-mode
  :mode "\\.[px]?html?\\'"
  :mode "\\.\\(?:tpl\\|blade\\)\\(?:\\.php\\)?\\'"
  :mode "\\.erb\\'"
  :mode "\\.[lh]?eex\\'"
  :mode "\\.sface\\'"
  :mode "\\.jsp\\'"
  :mode "\\.as[cp]x\\'"
  :mode "\\.ejs\\'"
  :mode "\\.hbs\\'"
  :mode "\\.mustache\\'"
  :mode "\\.svelte\\'"
  :mode "\\.twig\\'"
  :mode "\\.jinja2?\\'"
  :mode "\\.eco\\'"
  :mode "wp-content/themes/.+/.+\\.php\\'"
  :mode "templates/.+\\.php\\'"
  :hook (web-mode . lsp-deferred)
  :hook (web-mode . smartparens-mode))

(use-package cc-mode
  :mode ("\\.mm\\'" . objc-mode)
  ;; Use `c-mode'/`c++-mode'/`objc-mode' depending on heuristics
  :mode ("\\.h\\'" . +cc-c-c++-objc-mode)
  :hook (c-mode . lsp-deferred)
  :hook (c++-mode . lsp-deferred)
  :hook (objc-mode . lsp-deferred)
  :hook (c-mode . smartparens-mode)
  :hook (c++-mode . smartparens-mode)
  :hook (objc-mode . smartparens-mode))

(use-package modern-cpp-font-lock
  :hook (c++-mode . modern-c++-font-lock-mode))

(use-package cmake-mode)

(use-package company-cmake
  :straight nil)

(use-package demangle-mode
  :hook llvm-mode)

(use-package glsl-mode)

(use-package company-glsl  ; for `glsl-mode'
  :straight t
  :after glsl-mode)

(use-package rustic
  :mode ("\\.rs$'" . rustic-mode)
  :hook (rustic-mode . smartparens-mode)
  :hook (rustic-mode . lsp-deferred))

(use-package yaml-mode
  :hook (yaml-mode . lsp-deferred)
  :hook (yaml-mode . smartparens-mode))

(use-package json-mode
  :hook (json-mode . lsp-deferred)
  :hook (json-mode . smartparens-mode))

(use-package org-mode
  :straight nil
  :hook (org-mode . org-indent-mode)
  :hook (org-mode . prettify-symbols-mode)
  :general
  (:states 'normal :keymaps 'org-mode-map
           ;; remove maps so navigation works
           "C-h" nil
           "C-j" nil
           "C-k" nil
           "C-l" nil
           (sp/localleader "t") '(org-todo :which-key "todo")
           (sp/localleader "n") '(org-babel-tangle :which-key "tangle")))

(use-package org-superstar ; "prettier" bullets
  :hook (org-mode . org-superstar-mode)
  :config
  ;; Make leading stars truly invisible, by rendering them as spaces!
  (setq org-superstar-leading-bullet ?\s
        org-superstar-leading-fallback ?\s
        org-hide-leading-stars nil
        org-superstar-todo-bullet-alist
        '(("TODO" . 9744)
          ("[ ]"  . 9744)
          ("DONE" . 9745)
          ("[X]"  . 9745))))

(use-package org-fancy-priorities ; priority icons
  :hook (org-mode . org-fancy-priorities-mode)
  :hook (org-agenda-mode . org-fancy-priorities-mode)
  :config (setq org-fancy-priorities-list '( (all-the-icons-insert-material "flag") (all-the-icons-insert-octicon "arrow-up") "■")))

(use-package org-appear
  :hook (org-mode . org-appear-mode)
  :straight t)

(setq gc-cons-threshold (* 2 1000 1000))
